/* Snake V2 - Rafael Morais */
window.js = (function () {

    var config = {
        fieldCol: 10,
        fieldRow: 10,
        direction: 39,  //38 up //40 down //37 left //39 right
        block: false,
        snake: {
            head: {
                col: 0,
                row: 0
            },
            body: [],
            ass: {
                col: 0,
                row: 0
            }
        },
        food: {
            col: 0,
            row: 0
        },
        score: 0
    };

    function addClass (elem, classe) {
        var classes = elem.className.split(' ');
        var getIndex = classes.indexOf(classe);
      
        if (getIndex === -1) {
            classes.push(classe);
            elem.className = classes.join(' ');
        }
    }
      
    function delClass (classe, elemen) {
        if(!elemen)
            elemen = document.querySelector('.'+ classe);

        var classes = elemen.className.split(' ');
        var getIndex = classes.indexOf(classe);
      
        if (getIndex > -1) 
            classes.splice(getIndex, 1);

        elemen.className = classes.join(' ');
    }

    function getSelector(colrow, n){
        return (colrow == 'col' ? '[data-col="' : '[data-row="') + n + '"]';
    }

    function getRandomCoords (){
        var col, row;
        while (!col || col < 1 || col > config.fieldCol) {
            col = Math.floor(Math.random() * config.fieldCol);
        }

        while (!row || row < 1 || row > config.fieldCol) {
            row = Math.floor(Math.random() * config.fieldCol);
        }

        return {
            col: col,
            row: row
        };
    }

    function contains(array, n) {
        return array.indexOf(n) !== -1;
    }


   

    function setSnakeBody (born, oldAss) {        
        //Head
        var pos = document.querySelector(getSelector('col', config.snake.head.col)).querySelector(getSelector('row', config.snake.head.row));
        if (!born)
            delClass('head')
        addClass(pos, 'head');

        //Body
        for (var i = 0; i < config.snake.body.length; i++){
            var body = config.snake.body[i];
            pos = document.querySelector(getSelector('col', body.col)).querySelector(getSelector('row', body.row));
            addClass(pos, 'body');
        }
        
        //Ass
        pos = document.querySelector(getSelector('col', config.snake.ass.col)).querySelector(getSelector('row', config.snake.ass.row));
        if (!born)
            delClass('body', pos)
        addClass(pos, 'ass');

        //OldAss
        if (!born){
            pos = document.querySelector(getSelector('col', oldAss.col)).querySelector(getSelector('row', oldAss.row));
            delClass('ass', pos)
        }
        config.block = false;
    }

    function runSnake (coor, callback) {
        eat ();
        var aux = {}, next = coor;
        
        for (var i = 0; i < config.snake.body.length; i++){
            aux.row = config.snake.body[i].row;
            aux.col = config.snake.body[i].col;

            config.snake.body[i].row = next.row;
            config.snake.body[i].col = next.col;
            next.col = aux.col;
            next.row = aux.row;
        }
        
        aux.row = config.snake.ass.row;
        aux.col = config.snake.ass.col;
        config.snake.ass.col = next.col;
        config.snake.ass.row = next.row;
        
        if(callback && typeof(callback) === 'function')    
            callback(false, aux);
    }

    function setScore (){
        config.score += 10;
        document.querySelector('#score').textContent = config.score;
    }

    function eat () {
        if (config.food.col !== config.snake.head.col || config.food.row !== config.snake.head.row)
            return;

        setScore();

        var pos = document.querySelector(getSelector('col', config.food.col)).querySelector(getSelector('row', config.food.row));
        delClass('food', pos);

        config.snake.body.push({
            col: config.food.col,
            row: config.food.row
        });
        makeFood();
    }

    function toLeft () {
        var next = {};
        next.col = config.snake.head.col;
        next.row = config.snake.head.row;

        if (config.snake.head.col - 1 === 0)
            config.snake.head.col = config.fieldCol;
        else
            config.snake.head.col -= 1;
        
        runSnake(next, setSnakeBody);
    }

    function toRight () {
        var next = {};
        next.col = config.snake.head.col;
        next.row = config.snake.head.row;

        if (config.snake.head.col + 1 > config.fieldCol)
            config.snake.head.col = 1;
        else
            config.snake.head.col += 1;

        runSnake(next, setSnakeBody);
    }

    function toUp () {
        var next = {};
        next.col = config.snake.head.col;
        next.row = config.snake.head.row;

        if (config.snake.head.row - 1 === 0)
            config.snake.head.row = config.fieldRow;
        else
            config.snake.head.row -= 1;

        runSnake(next, setSnakeBody);
    }

    function toDown () {
        var next = {};
        next.col = config.snake.head.col;
        next.row = config.snake.head.row;

        if (config.snake.head.row + 1 > config.fieldRow)
            config.snake.head.row = 1;
        else
            config.snake.head.row += 1;

        runSnake(next, setSnakeBody);
    }

    function getDirection () {
        switch (config.direction){
            case 37: toLeft(); break;
            case 38: toUp(); break;
            case 39: toRight(); break;
            case 40: toDown(); break;
            default: break;
        }
    }

    function validFood (elem) {
        if(!elem)
            return false;

        var classes = elem.className.split(' ');
        for (var i = 0; i < classes.length; i++)
        {
            if(contains(['head', 'body', 'ass'], classes[i]))
                return false;
        }
        return true;
    }

    function makeFood (){
        var pos, coor = {};

        while (!validFood(pos)){
            coor = getRandomCoords();
            pos = document.querySelector(getSelector('col', coor.col)).querySelector(getSelector('row', coor.row));
        }
        config.food.col = coor.col;
        config.food.row = coor.row;
        addClass(pos, 'food');
    }

    function startMove (){
        var interval = setInterval(getDirection, 300);
        makeFood();
        // setTimeout(function (){
        //     clearInterval (interval);
        // }, 3000);
    }

    function bornUp (callback){
        config.snake.head = getRandomCoords();
        
        var body = {
            row: config.snake.head.row
        };

        if (config.snake.head.col - 1 < 1)
            body.col = config.fieldCol;
        else 
            body.col = config.snake.head.col - 1;

        config.snake.body.push(body);

        config.snake.ass = {
            row: config.snake.head.row
        };

        switch (config.snake.head.col - 2){
            case 0: config.snake.ass.col = config.fieldCol; break;
            case -1: config.snake.ass.col = config.fieldCol - 1; break;
            default: config.snake.ass.col = config.snake.head.col - 2; break;
        }

        setSnakeBody(true);

        if(callback && typeof(callback) === 'function')    
            callback();
    }

    function doField (callback, callback1){
        var field = document.querySelector('.field');

        for(var i = 1; i <= config.fieldCol; i++)
            field.innerHTML += '<div class="col" data-col="'+ i +'"></div>';

        for(var i = 1; i <= config.fieldCol; i++){
            var col = document.querySelector(getSelector('col', i));

            for(var j = 1; j <= config.fieldRow; j++)
                col.innerHTML += '<div class="row" data-row="'+ j +'"></div>';
        }

        if(callback && typeof(callback) === 'function')    
            callback(callback1);
    }

    function setDirection (direction) {
        config.block = true;
        if (contains([38, 40], direction) && contains([37, 39], config.direction))
            config.direction = direction;

        else if (contains([37, 39], direction) && contains([38, 40], config.direction))
            config.direction = direction;
    }

    var init = function(){
        doField(bornUp, startMove);
        
        document.addEventListener('keyup', function (e) {
            e = e || window.event;
            var code = e.which || e.keyCode;

            if (contains([37, 38, 39, 40], code) && !config.block)
                setDirection(code);
        });
    };

    return {
        config: config,
        init: init
    };

})();
